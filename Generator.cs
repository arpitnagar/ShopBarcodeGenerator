﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using System.Drawing.Imaging;
using MessagingToolkit.Barcode.Multi;
using MessagingToolkit.Barcode;
using MessagingToolkit.Barcode.Common;
using Spire.Barcode;
using System.IO;

namespace BarcodeGen
{
    public class Generator
    {
        private MultiFormatReader barcodeReader;
        private MultiFormatWriter barcodeWriter;

        public Generator()
        {
            barcodeReader = new MultiFormatReader();
            barcodeWriter = new MultiFormatWriter();
        }

        public Bitmap CreateBarcode(String barcodeText, BarCodeType barcodeFormat, string tagline, int width, int height)
        {

            if (!string.IsNullOrEmpty(barcodeText))
            {
                try
                {
                    BarcodeSettings.ApplyKey("7AQ86IQ02CS-NT4HE-G1K84-2RI2J");
                    BarcodeSettings bs = new BarcodeSettings();
                    bs.Type = barcodeFormat;
                    bs.Data = barcodeText;
                    bs.ShowTextOnBottom = true;
                    bs.TopText = tagline;
                    bs.ShowTopText = true;
                    bs.TextFont = new Font("calibri", 18f, FontStyle.Bold);
                    BarCodeGenerator bg = new BarCodeGenerator(bs);
                    var bmp =  bg.GenerateImage();
                    return new Bitmap(bmp);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return null;
        }




        private unsafe Bitmap ConvertByteMatrixToImage(ByteMatrix bm)
        {
            Bitmap image = CreateGrayscaleImage(bm.Width, bm.Height);
            BitmapData sourceData;

            sourceData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, image.PixelFormat);
            int width = sourceData.Width;
            int height = sourceData.Height;
            int srcOffset = sourceData.Stride - width;
            byte* src = (byte*)sourceData.Scan0.ToPointer();
            for (int y = 0; y < height; y++)
            {
                // for each pixel
                for (int x = 0; x < width; x++, src++)
                {
                    *src = (byte)bm.Array[y][x];
                }
                src += srcOffset;
            }

            image.UnlockBits(sourceData);
            return image;
        }

        private Bitmap CreateGrayscaleImage(int width, int height)
        {
            // create new image
            Bitmap image = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
            // set palette to grayscale
            // get palette
            ColorPalette cp = image.Palette;
            // init palette
            for (int i = 0; i < 256; i++)
            {
                cp.Entries[i] = Color.FromArgb(i, i, i);
            }
            // set palette back
            image.Palette = cp;
            // return new image
            return image;
        }
    }
}